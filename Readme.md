This library helps you run pipelines of jobs that depend on each other.
Its modularity allows to run jobs either locally, on a cluster or other remote computing resources.