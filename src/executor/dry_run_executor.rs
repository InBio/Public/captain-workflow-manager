use crate::ExpandedDescr;

use super::*;
pub struct DryRunExecutorBuilder {}

impl ExecutorBuilder for DryRunExecutorBuilder {
    type Executor = DryRunExecutor;

    fn init<J: JobUnit>(
        self,
    ) -> (
        Self::Executor,
        Sender<ToRun<J>>,
        Sender<ExecutorResult>,
        Receiver<ExecutorResult>,
    ) {
        let to_run_chan = crossbeam_channel::unbounded::<ToRun<J>>();
        let done_chan = crossbeam_channel::unbounded();
        let thread_handle = {
            let rcvr = to_run_chan.1.clone();
            let sndr = done_chan.0.clone();
            thread::Builder::new()
                .spawn(move || {
                    for to_run in rcvr.into_iter() {
                        let j_idx = to_run.job_idx;

                        let stdout = io::stdout();
                        {
                            let mut lock = stdout.lock();
                            write!(lock, "Would submit job: {:#?} interpreted as: {:#?} ", to_run.job_unit, ExpandedDescr(to_run.job_unit())).unwrap();
                            writeln!(lock, "\n").unwrap();
                        }

                        sndr.send(ExecutorResult::new_ok(j_idx)).unwrap()
                    }
                })
                .unwrap()
        };
        (
            DryRunExecutor {
                thread: thread_handle,
            },
            to_run_chan.0,
            done_chan.0,
            done_chan.1,
        )
    }
}

pub struct DryRunExecutor {
    thread: JoinHandle<()>,
}

impl Executor for DryRunExecutor {
    fn join(self) -> Vec<std::thread::Result<()>> {
        vec![self.thread.join()]
    }
}
